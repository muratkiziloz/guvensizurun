import './modules'
console.log(`app.js has loaded!`)

let app = new Vue({
  el: '#app',
  data: {
    message: 'Hello Vue!'
  },
  methods: {
    clickMe: function () {
      console.log('clickme')
    }
  }
});
